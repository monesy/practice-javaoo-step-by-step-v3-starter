package ooss;

public class Student extends Person {

    protected Integer klass;
    protected boolean Leader;


    public Student(int id, String name, int age) {
        super(id, name, age);
        this.status = "student";
    }

    @Override
    public String introduce() {
        if (this.klass != 0) {
            if (this.Leader) {
                return String.format("My name is %s. I am %d years old. I am a student. I am the leader of class %d.", this.name, this.age, this.klass);
            } else {
                return String.format("My name is %s. I am %d years old. I am a student. I am in class %d.", this.name, this.age, this.klass);
            }
        } else {
            return String.format("My name is %s. I am %d years old. I am a student.", this.name, this.age);
        }

    }

    public void join(Klass klass) {
        this.klass = klass.number;
    }

    public boolean isIn(Klass klass) {
        if (this.klass == klass.number) {
            return true;
        } else {
            return false;
        }
    }

    public void say(Student student) {
        System.out.println("I am " + this.name + ", student of Class " + this.klass + ". I know " + student.name + " become Leader.");
    }

}
