package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {
    protected int number;
    protected String leaderName;
    protected List<Person> personList = new ArrayList<>();

    public Klass(int number) {
        this.number = number;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void assignLeader(Student student) {
        if (student.klass == null) {
            System.out.println("It is not one of us.");
        } else {
            student.Leader = true;
            this.leaderName = student.getName();
            for (int i = 0; i < personList.size(); i++) {
                personList.get(i).say(student);
            }
        }
    }

    public boolean isLeader(Student student) {

        return student.Leader;
    }

    public void attach(Person person) {
        person.attach = true;
        personList.add(person);

    }
}
