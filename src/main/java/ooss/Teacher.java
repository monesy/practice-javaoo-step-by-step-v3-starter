package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    protected List<Integer> klass = new ArrayList<>();


    public Teacher(int id, String name, int age) {
        super(id, name, age);
        this.status = "teacher";
    }

    @Override
    public String introduce() {

        if (this.klass.size() == 0) {
            return String.format("My name is %s. I am %d years old. I am a teacher.", this.name, this.age);
        } else {
            return "My name is " + this.name + "." + " I am " + this.age + " years old. I am a teacher. I teach Class " + this.klass.stream().map(Object::toString).collect(Collectors.joining(", ")) + ".";
        }

    }


    public void assignTo(Klass klass) {
//        if (this.klass == null) {
//            this.klass = null;
//        }
        this.klass.add(klass.getNumber());
    }

    public boolean belongsTo(Klass klass) {
        if (this.klass.contains(klass.number)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isTeaching(Student student) {
        if (this.klass.contains(student.klass)) {
            return true;
        }
        return false;
    }

    public void say(Student student) {
        System.out.println("I am " + this.name + ", teacher of Class " + student.klass + ". I know " + student.name + " become Leader.");
    }


}
